from loguru import logger
from django.apps import AppConfig

from django.db.models.signals import post_migrate


class AccountsConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'apps.accounts'

    def ready(self) -> None:
        post_migrate.connect(initialization_account, sender=self)
        logger.info(">>> Accounts module loading success")


def initialization_account(sender, **kwargs):
    load_account_models_data()


def load_account_models_data():
    try:
        from .models import User, Role
        if Role.objects.all().count() == 0:
            role_list = [
                Role(role_name="管理员"),
                Role(role_name="运维人员"),
                Role(role_name="普通人员")
            ]

            Role.objects.bulk_create(role_list, batch_size=100)

        if not  User.objects.filter().first():
            acc = User.objects.create(
                username="admin",
                password="f+PGQ7q6FJBAE/Cn8V4mcAzPUds9mALzWvAOHvXz8V8MuZfQWVsXzDi2E1DoH3UD01JN9K9lgQ7LHMFHhdZ6/ip7I6XJLsYp+f0hygkoOS2pMHgI/5b8QhHzaQX539YNXc+GwzmCylxWGaHiGDR2RGRwIV+R/z4+hJe2uI8j6nnbcYAlveEiQDvmnqVBuPUK+80zt0z6/8T0BBI0JD58pVoCpO2YOgM+ypRIeGpKDyEQeKpyQUnCrDnE7j/MeCkUYWKePn8FMX5+As57iz4BXzP/K1JN0iOqcE9ssZnoP51fH9mZpe0F+jjLcG4e2Ttt6dSNcQTHqe8DafFGoDkE+w==",
                is_admin=True,
                is_agree=True,
                description="系统管理员"
                )
            acc.role.add(*[1,])
            acc.save()
    except Exception as e:
        pass

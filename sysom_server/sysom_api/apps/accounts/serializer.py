from rest_framework import serializers
from loguru import logger

from . import models
from lib.utils import JWT
from django.conf import settings
from apps.host.models import HostModel
from lib.utils import human_datetime, datetime
from django.utils import timezone
from datetime import datetime
from datetime import timedelta

LOGIN_FAILURE_LOCK_TIME = 20  # 账户被锁定的时间（分钟）
LOGIN_FAILURE_THESHOLD = 5   # 登录失败次数阈值
LOGIN_FAILURE_TIME_THESHOLD = 5  #账号在xx分钟内登录失败次数

def lock_account(user):
    user.last_locked_time = human_datetime()
    user.save()

def unlock_account(user):
    user.login_attempts = 0
    user.last_locked_time = None
    user.first_error_time =  None
    user.save()


class UpdateUserSerializer(serializers.ModelSerializer):
    is_admin = serializers.BooleanField(required=True, write_only=True)
    role = serializers.ListField(read_only=True)
    class Meta:
        model = models.User
        exclude = ['password']


class UserListSerializer(serializers.ModelSerializer):
    role = serializers.SerializerMethodField()
    host_count = serializers.SerializerMethodField()

    class Meta:
        model = models.User
        exclude = ['password']

    def get_role(self, instance: models.User):
        roles = instance.role.all()
        return RoleListSerializer(instance=roles, many=True).data

    def get_host_count(self, obj):
        return HostModel.objects.filter(created_by=obj.pk).count()


class AddUserSerializer(serializers.ModelSerializer):
    username = serializers.CharField(error_messages={'required': "用户名必填"})
    password = serializers.CharField(error_messages={'required': "密码必填"}, required=True)
    role = serializers.ListField(required=False, write_only=True)

    class Meta:
        model = models.User
        # exclude = ('description', )
        fields = "__all__"

    def validate_username(self, attr):
        try:
            if len(attr) > 128:
                raise serializers.ValidationError("用户名长度不能大于128，用户新增失败!")
            models.User.objects.get(username=attr)
        except models.User.DoesNotExist:
            return attr
        raise serializers.ValidationError("用户已存在!")

    #def validate_password(self, attr):
    #    return models.User.make_password(attr)

    def validate_role(self, attr):
        return models.Role.objects.filter(id__in=attr)

    def save(self, **kwargs):
        count = models.User.objects.all().count()
        validated_data = {**self.validated_data, **kwargs}
        if self.instance is not None:
            is_admin = validated_data.get('is_admin', None)
            if is_admin:
                roles = models.Role.objects.filter(role_name='管理员')
                validated_data.update({'role': roles})

            self.instance: models.User = self.update(self.instance, validated_data)
            assert self.instance is not None, (
                '`update()` did not return an object instance.'
            )
        else:
            instance: models.User = self.create(validated_data)
            if count == 0:
                role = models.Role.objects.filter(role_name='管理员')
                instance.role.add(*role)
                instance.is_admin = True
            else:
                is_admin = validated_data.get("is_admin", None)
                if is_admin:
                    role = models.Role.objects.filter(role_name='管理员')
                else:
                    role = models.Role.objects.filter(role_name='普通人员')
                instance.role.add(*role)
            instance.save()
        return self.instance


class UserAuthSerializer(serializers.ModelSerializer):
    username = serializers.CharField(error_messages={'required': "用户名必填"})
    password = serializers.CharField(error_messages={'required': "密码名必填"})

    class Meta:
        model = models.User
        fields = ("username", "password")

    def validate(self, attrs):
        user = models.User.objects.get(username=attrs['username'])

        if user.login_attempts >= LOGIN_FAILURE_THESHOLD:
            if  user.last_locked_time and datetime.now() - datetime.strptime(str(user.last_locked_time),'%Y-%m-%d %H:%M:%S') > timedelta(minutes=LOGIN_FAILURE_LOCK_TIME):
                unlock_account(user)
            else:
                raise serializers.ValidationError('账户已被锁定，请稍候再试。')

        if not user.verify_password(attrs['password']):
            if user.login_attempts == 0:
                user.login_attempts = 1
                user.first_error_time = human_datetime()
                user.save()
                raise serializers.ValidationError("用户名或密码不正确！")
            else:
                if datetime.now() - datetime.strptime(str(user.first_error_time),'%Y-%m-%d %H:%M:%S') > timedelta(minutes=LOGIN_FAILURE_TIME_THESHOLD):
                    user.login_attempts = 1
                    user.first_error_time = human_datetime()
                else:
                    user.login_attempts = user.login_attempts + 1
                user.save()
            if user.login_attempts >= LOGIN_FAILURE_THESHOLD:
                lock_account(user)
                raise serializers.ValidationError('账户已被锁定，请稍候再试。')
            raise serializers.ValidationError("用户名或密码不正确！")
        return attrs

    def validate_username(self, attr):
        try:
            models.User.objects.get(username=attr)
        except models.User.DoesNotExist:
            raise serializers.ValidationError("用户名或密码不正确！")
        return attr

    def create_token(self):
        user = models.User.objects.get(username=self.data.get('username'))
        token = JWT._encode({'id': user.id, 'username': user.username})
        return user, token


class RoleListSerializer(serializers.ModelSerializer):
    permissions = serializers.SerializerMethodField()

    class Meta:
        model = models.Role
        exclude = ['deleted_at']

    def get_permissions(self, instance: models.Role):
        permissions = instance.permissions.all()
        return PermissionListSerializer(instance=permissions, many=True).data


class AddRoleSerializer(serializers.ModelSerializer):
    permissions = serializers.ListField(required=False, write_only=True)

    class Meta:
        model = models.Role
        fields = "__all__"


class PermissionListSerializer(serializers.ModelSerializer):
    method = serializers.SerializerMethodField()

    class Meta:
        model = models.Permission
        exclude = ("deleted_at", )

    def get_method(self, instance: models.Permission):
        return instance.get_method_display()


class AddPermissionSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Permission
        exclude = ('deleted_at', )


class HandlerLoggerListSerializer(serializers.ModelSerializer):
    request_option = serializers.CharField(source='get_request_option_display', read_only=True)
    username = serializers.CharField(source='user.username', read_only=True)

    class Meta:
        model = models.HandlerLog
        exclude = ('deleted_at', 'user')

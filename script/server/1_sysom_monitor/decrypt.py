#!/usr/bin/python3
import base64
from Crypto.Cipher import PKCS1_v1_5
from Crypto import Random
from Crypto.PublicKey import RSA

private_key_path = "/usr/local/sysom/server/target/conf/crypto_private_key.pem"
password_path = "/usr/local/sysom/init_scripts/server/1_sysom_monitor/password_encrypted.txt"

def read_path(path) -> bytes:
    with open(path, "rb") as x:
        b = x.read()
        return b

def decryption(text_encrypted_base64: str, private_key: bytes):
    # 字符串指定编码（转为bytes）
    #text_encrypted_base64 = text_encrypted_base64.encode('utf-8')
    # base64解码
    text_encrypted = base64.b64decode(text_encrypted_base64)
    # 构建私钥对象
    cipher_private = PKCS1_v1_5.new(RSA.importKey(private_key))
    # 解密（bytes）
    text_decrypted = cipher_private.decrypt(text_encrypted, Random.new().read)
    # 解码为字符串
    text_decrypted = text_decrypted.decode()
    return text_decrypted


if __name__ == '__main__':
    private_key = read_path(private_key_path)
    password_encrypted = read_path(password_path)
    password_decrypted = decryption(password_encrypted, private_key)

    print(password_decrypted)




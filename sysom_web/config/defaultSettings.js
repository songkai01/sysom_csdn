const Settings = {
    navTheme: 'realDark',
    primaryColor: '#0070e0',
    layout: 'top',
    contentWidth: 'Fluid',
    fixedHeader: true,
    fixSiderbar: true,
    // title: 'System Operation&Maintenance',
    title: 'X2Keyarch操作系统迁移软件 V2.0',
    logo: null,
    footerLink: {
    }
};
export default Settings;

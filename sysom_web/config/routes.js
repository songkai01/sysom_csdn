export default [
  {
    path: '/user',
    layout: false,
    routes: [
      {
        path: '/user',
        routes: [
          {
            name: 'login',
            path: '/user/login',
            component: './user/Login',
          },
        ],
      },
      {
        component: './404',
      },
    ],
  },
  {  //主机管理菜单
    path: '/host',
    name: 'host',
    routes: [
      {
        path: '/host',
        redirect: '/host/list',
      },
      {
        path: '/host/list',
        name: 'list',
        component: './host/List',
      },
      {
        path: '/host/cluster',
        name: 'cluster',
        component: './host/cluster',
      },
      {
        path: '/host/terminal/:ip?',
        component: './host/Terminal',
      }
    ],
  },
  {  //操作系统迁移
    path: '/migrate',
    name: 'migrate',
    routes: [
      {
        path: '/migrate',
        redirect: '/migrate/implement',
      },
      {
        path: '/migrate/assess',
        name: 'assess',
        component: './migrate/assess',
      },
      {
        path: '/migrate/report/:id?',
        // name: 'report',
        hideInBreadcrumb: true,
        component: './migrate/assessReport',
      },
      {
        path: '/migrate/implement',
        name: 'implement',
        hideInBreadcrumb: true,
        component: './migrate/implement',
      },
    ]
  },
  { //监控中心
    path: '/monitor',
    name: 'monitor',
    hideInMenu: true, //隐藏监控中心
    routes: [
      {
        path: '/monitor',
        redirect: '/monitor/migration',
      },
      {
        path: 'migration',
        name: 'migration',
        hideInBreadcrumb: true,
        component: './Monitor/MigrationDashboard',
      },
      {
        path: 'migration/:host?',
        component: './Monitor/MigrationDashboard',
      },
      {
        component: './404',
      },
    ],
  },
  { //诊断中心
    path: '/diagnose',
    name: 'diagnose',
    // hideInMenu: true,
    routes: [
      {
        path: '/diagnose',
        redirect: '/diagnose/oscheck',
      },
      {
        path: '/diagnose/oscheck',
        name: 'oscheck',
        component: './diagnose/oscheck',
      },
      {
        path: '/diagnose/cpu',
        name: 'cpu',
        routes: [
          {
            path: '/diagnose/cpu',
            redirect: '/diagnose/cpu/loadtask',
          }
        ]
      },
      {
        path: '/diagnose/storage',
        name: 'storage',
        routes: [
          {
            path: '/diagnose/storage',
            redirect: '/diagnose/storage/iolatency',
          }
        ]
      },
      {
        path: '/diagnose/net',
        name: 'net',
        routes: [
          {
            path: '/diagnose/net',
            redirect: '/diagnose/net/pingtrace',
          },
        ]
      },
      {
        path: '/diagnose/memory',
        name: 'memory',
        routes: [
          {
            path: '/diagnose/memory',
            redirect: '/diagnose/memory/memgraph',
          }
        ]
      },
      {
        path: '/diagnose/detail/:task_id?',
        layout: false,
        component: "./diagnose/detail"
      }
    ],
  },
  { //热补丁中心
    path: '/hotfix',
    name: 'hotfix',
    routes: [
      {
        path: '/hotfix/formal_hotfix',
        name: 'formal',
        component: './hotfix/FormalHotfixList'
      },
      {
        path: '/hotfix',
        redirect: '/hotfix/make',
      },
      {
        path: '/hotfix/make',
        name: 'make',
        component: './hotfix/Make',
      },
      {
        path: '/hotfix/hotfix_log/:id?',
        component: './hotfix/HotfixLog'
      },
      {
        path: '/hotfix/verion_config',
        name: 'versionconf',
        component: './hotfix/VersionConf'
      }
    ]
  },
  { //日志中心
    path: '/journal',
    name: 'journal',
    routes: [
      {
        path: '/journal',
        redirect: '/journal/alarm',
      },
      {
        path: '/journal/alarm',
        name: 'alarm',
        component: './journal/Alarm',
      },
    ],
  },
  {
    path: '/account',
    access: 'canAdmin',
    routes: [
      {
        path: '/account/center',
        name: 'list',
        component: './account/List',
      },
    ]
  },
  {
    path: '/',
    redirect: '/host/list',
  },
  {
    component: './404',
  },
];

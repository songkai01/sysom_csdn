import crypto from 'crypto';
import { JSEncrypt } from 'jsencrypt'

function pbkdf2_sha256(password, salt, iterations, keylen) {
  return new Promise((resolve, reject) => {
    crypto.pbkdf2(password, salt, iterations, keylen, 'sha256', (err, derivedKey) => {
      if (err) return reject(err);
      resolve(derivedKey.toString('hex'));
    });
  });
}

//rsa加密算法函数
function rsaEncrypt(password){
  const PUBLICKEY = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAzodlcwASx1Zg00i0J58s\
  mMso0usG3ZlB+ip/gzt2ZZP2K61iooAozLBW2f7BONhfm+fEzmSg+n8GSDJsjqsm\
  ePrYbuSUA3+5Fu598k71cajlpUpKwGRFqNl4Geb5UhPFYI55LEto6JUi5/rkh8Pl\
  ak/hiEgBsEITg2icJVaENxANtaODJEVrbFz0djGSchGeEn3sr88EIwqjkaOLaw1E\
  WOHoDWNi/BKel9XKYCHLtv5ALRHNUiM8qe0ENlXlpgcBX09uHX5SK7VpeYDAfUwW\
  hK5NYk9fNHu+ulXopESUYIQLLG0qfIEhKORl1EU8yv1i9KzlDmbRUU9QdxeZiILV\
  CwIDAQAB";
  var encrypt = new JSEncrypt();
  encrypt.setPublicKey(PUBLICKEY);
  return encrypt.encrypt(password);
}

// pbkdf2_sha256加密函数
async function useSha256(password){
  const salt = 'sysom/passwd'; //加密盐值，随机的字符串
  const iterations = 100000;  //加密迭代次数
  const keylen = 32;  //加密后的长度
  const derivedKey = await pbkdf2_sha256(password, salt, iterations, keylen); //加密后的密码的密文
  return derivedKey;
}

export {useSha256, rsaEncrypt};
import react, {useState,useEffect} from 'react';
import ProForm, {ProFormSelect, ProFormRadio, ProFormText, ProFormCheckbox} from '@ant-design/pro-form';
import {Button,message, Checkbox} from 'antd';
import {useRequest} from 'umi';
import ProCard from '@ant-design/pro-card';
import {queryAssessHost,queryStartAssess} from '../../../service'
// querySqlFile
import './StAssessmentForm.less';

export default (props) => {
  const [hostList,setHostList] = useState([]);
  const [sqlFileList,setSqlFileList] = useState([]);
  // Repo配置选择项
  const [repoType, setRepoType] = useState(false);
  // 评估应用是否展示
  const [appType, setAppType] = useState(false);
  // 全选按钮状态
  const [allSelected, setAllSelected] = useState(false);
  const [selectedValues, setSelectedValues] = useState([]);

  const [form] = ProForm.useForm();

  const handleSelectChange = (value) => {
    setSelectedValues(value);
    form.setFieldsValue({
      ip: value
    });
    if(value.length != hostList.length){
      setAllSelected(false)
    }

    if(value.length == hostList.length){
      setAllSelected(true)
    }

  };

  const handleSelectAllChange = (event) => {
    const { checked } = event.target;
    setAllSelected(checked);

    if (checked) {
      const valueList = hostList.map((option) => {
        return option.value;
      })
      setSelectedValues(valueList);
      form.setFieldsValue({
        ip: valueList
      });
    } else {
      setSelectedValues([]);
    }
  };
  
  const initialValues = {
    repo_type: 'public',
    version: 'KOS 5.8',
    ass_type: ['mig_imp']
  };

  useEffect(()=>{
    getAssessHost();
    // getSqlFile();
  },[]);

  const getAssessHost = async () => {
    const {data} = await queryAssessHost();
    let arr = [];
    if(data?.length > 0){
      data.forEach((i)=>{
        arr.push({label: i.ip,value: i.ip})
      })
    }
    setHostList(arr);
  }

  // const getSqlFile = async () => {
  //   const {data} = await querySqlFile();
  //   setSqlFileList(data?data:[]);
  // }

  const handleRepo = (e) => {
    if(e.target.value === "public"){
      setRepoType(false);
    }else if(e.target.value === "private"){
      setRepoType(true);
    }
  }

  const handleType = (e) => {
    let isShow = false;
    e?.length > 0 && e.forEach((i)=>{
      if(i === 'mig_app'){
        isShow = true;
      }
    });
    setAppType(isShow);
  }

  // 开始评估的接口
  const { loading, error, run } = useRequest(queryStartAssess, {
    manual: true,
    onSuccess: (result, params) => {
      message.success('开始评估成功')
      // 开始评估成功后刷新列表
      props?.success();
    },
    onError:(data)=>{
      console.log('请求错误',error,)
    },
  });

    return (
      <ProCard>
        <ProForm form={form}
          onFinish={async (values) => {
            run(values);
          }}
          submitter={{
            submitButtonProps: {
              style: {
                display: "none",
              },
            },
            resetButtonProps: {
              style: {
                display: "none",
              },
            },
          }}
          layout={"horizontal"}
          autoFocusFirstInput
          initialValues={initialValues}
        >
          <ProForm.Group>
            <ProFormSelect
              name="ip"
              label="选择主机"
              width="sm"
              options={hostList}
              fieldProps={{
                value: selectedValues,
                onChange: handleSelectChange,
                mode: 'multiple'
              }}
              placeholder="请选择主机"
              rules={[
                // {
                //   validator: (_, value) => {
                //     console.log('-------------checkValue------', value)
                //     return value.length > 20 ? Promise.reject('分类名称不能超过20个字符') : Promise.resolve();
                //   }
                // },
                { required: true, message: "主机不能为空", type: "array" },
              ]}
            />
            <label style={{marginLeft: '-20px', width: '60px', display: 'inline-block', verticalAlign: 'middle'}}>
              <Checkbox
                checked={allSelected}
                onChange={handleSelectAllChange}
              >
              全选
              </Checkbox>
              
            </label>
            <ProFormSelect
              name="version"
              label="迁移版本"
              width="sm"
              options={[
                {
                  label: "KOS 5.8",
                  value: "KOS 5.8",
                },
              ]}
              placeholder="请选择迁移版本"
              rules={[{ required: true, message: "迁移版本不能为空" }]}
            />
            {/* <ProFormSelect
              name="sqlfile"
              label="数据文件"
              width="sm"
              options={sqlFileList}
              placeholder="请选择数据文件"
              rules={[{ required: true, message: "数据文件不能为空" }]}
            /> */}
            <ProFormRadio.Group
              name="repo_type"
              label="Repo配置"
              options={[
                {
                  label: "公网地址",
                  value: "public",
                },
                {
                  label: "内网地址",
                  value: "private",
                },
              ]}
              onChange={handleRepo}
            />
            {repoType && (
              <ProFormText
                colProps={{ span: 24 }}
                name="repo_url"
                width='sm'
                label=' '
                colon={false}
                placeholder="请输内网地址"
                rules={[
                  { required: true, message: "内网地址不能为空"},
                ]}
              />
            )}
          </ProForm.Group>
          <ProForm.Group>
            <ProFormCheckbox.Group
              name="ass_type"
              label="选择评估"
              rules={[{ required: true, message: "评估不能为空" }]}
              onChange={handleType}
              options={[
                { label: '风险评估', value: 'mig_imp', disabled: true },
                { label: '系统评估', value: 'mig_sys' },
                { label: '硬件评估', value: 'mig_hard' },
                { label: '应用评估', value: 'mig_app' },
              ]}
            />
            {appType && (
              <ProFormText
                name="ass_app"
                width='sm'
                label=" "
                colon={false}
                placeholder="请输入评估应用"
                rules={[
                  { required: true, message: "评估应用不能为空"},
                ]}
              />
            )}
            <Button
              className="st_form_start"
              type="primary"
              htmlType="submit"
              // loading={loading}
            >
              开始评估
            </Button>
          </ProForm.Group>
        </ProForm>
      </ProCard>
    );
}

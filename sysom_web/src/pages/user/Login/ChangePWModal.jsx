import { useRef, useState } from "react";
import { Button, message } from "antd";
import {rsaEncrypt} from "@/utils/encryption"
import {
  ModalForm,
  ProFormText,
  ProFormSwitch,
  ProFormTextArea,
} from "@ant-design/pro-form";
import { FormattedMessage } from "umi";
import { ChangePassword } from "../../user/Login/service";

const changeAccountPWHandler = async (values) => {
  try {
    const result = await ChangePassword({ ...values });
    if (result.code === 200) {
      message.success("密码修改成功");
      return true;
    } else {
      return false;
    }
  } catch (error) {
    return false;
  }
};

const ChangePWModal = (props) => {
  const {loginChangePw , setLoginChangePw} = props;
  const passwordReg = /^(?![A-Za-z]+$)(?![A-Z\d]+$)(?![A-Z\W]+$)(?![a-z\d]+$)(?![a-z\W]+$)(?![\d\W]+$)[\s\S]{8,}$/
  

  const editAccountFormRef = useRef();
  return (
    <ModalForm
      title={
        <FormattedMessage
          id="pages.account.pw_modal_title"
          defaultMessage="Edit Account Info"
        />
      }
      visible={loginChangePw ? loginChangePw : false}
      width={500}
      formRef={editAccountFormRef}
      onFinish={async (values) => {
        //加密密码
        values.username = localStorage.getItem('username')
        values.row_password = rsaEncrypt(values.row_password)
        values.new_password = rsaEncrypt(values.new_password)
        values.new_password_again = rsaEncrypt(values.new_password_again)
        const r = await changeAccountPWHandler(values);
        if (r) {
          setLoginChangePw(false)
        }
        return r;
      }}
      modalProps={ {onCancel:()=>{setLoginChangePw(false)}}}
    >
      
      <div style={{display: "none"}}>
        <ProFormText.Password/>
      </div>
      <ProFormText.Password
        name="row_password"
        fieldProps={{
          autoComplete: 'off',
        }}
        preserve={false}
        label={
          <FormattedMessage
            id="pages.account.oldPW"
            defaultMessage="username"
          />
        }
        rules={[
          {
            required: true,
            message: (
              <FormattedMessage
                id="pages.login.row_password.required"
                defaultMessage="请输入原始密码！"
              />
            ),
          }
        ]}
      />
      <ProFormText.Password
        name="new_password"
        preserve={false}
        label={
          <FormattedMessage
            id="pages.account.newPW"
            defaultMessage="username"
          />
        }
        rules={[
          {
            required: true,
            message: (
              <FormattedMessage
                id="pages.login.new_password.required"
                defaultMessage="请输入新密码！"
              />
            ),
          },{
            pattern: passwordReg,
            message: '密码至少8位, 包括数字、大小写字母和特殊字符三种及以上'
          }
        ]}
      />
      <ProFormText.Password
        name="new_password_again"
        preserve={false}
        label={
          <FormattedMessage
            id="pages.account.newPWTwo"
            defaultMessage="username"
          />
        }
        rules={[
          {
            required: true,
            message: (
              <FormattedMessage
                id="pages.login.new_password_again.required"
                defaultMessage="请再次输入新密码！"
              />
            ),
          },{
            pattern: passwordReg,
            message: '密码至少8位, 包括数字、大小写字母和特殊字符三种及以上'
          },({ getFieldValue }) => ({
            validator(_, value) {
              if (!value || getFieldValue('new_password') === value) {
                return Promise.resolve();
              }
              return Promise.reject(new Error('两次密码不同'));
            },
          }),
        ]}
      />
      
      
    </ModalForm>
  );
};

export default ChangePWModal;

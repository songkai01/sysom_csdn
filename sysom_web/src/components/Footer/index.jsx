import { useIntl } from 'umi';
import { DefaultFooter } from '@ant-design/pro-layout';
import Settings from '../../../config/defaultSettings';

const Footer = () => {
  const intl = useIntl();
  const defaultMessage = intl.formatMessage({
    id: 'app.copyright.produced',
    defaultMessage: 'X2Keyarch操作系统迁移软件 V2.0',
  });
  const currentYear = new Date().getFullYear();

  let links = [];
  if (!!Settings.footerLink?.title && !!Settings.footerLink?.link) {
    links = [{
      ...Settings.footerLink,
      href: Settings.footerLink.link,
      key: Settings.footerLink.title
    }]
  }
  return (
    <DefaultFooter
      // copyright={`${currentYear} ${defaultMessage}`}
      copyright="浪潮电子信息产业股份有限公司"
      links={links}
    />
  );
};

export default Footer;
